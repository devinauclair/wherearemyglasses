using Toybox.Application;

class WhereAreMyGlassesApp extends Application.AppBase {
        var watchface;

    function initialize() {
        AppBase.initialize();
    }

    // onStart() is called on application start up
    function onStart(state) {
    }

    // onStop() is called when your application is exiting
    function onStop(state) {
    }

    // Return the initial view of your application here
    function getInitialView() {
        watchface = new WhereAreMyGlassesView();
        return [ watchface ];
    }

    // New app settings have been received so trigger a UI update
    function onSettingsChanged() {
        watchface.updateSettings();
    }
}

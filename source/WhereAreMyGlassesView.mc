using Toybox.Application;
using Toybox.Graphics;
using Toybox.Math;
using Toybox.System;
using Toybox.Time;
using Toybox.WatchUi;

class WhereAreMyGlassesView extends WatchUi.WatchFace {
    var colorBackground = 0x000000;
    var colorHour = 0x009F00;
    var colorMinute = 0xFFFFFF;
    var colorDate = 0xFFFFFF;
    var colorLine = 0xFFFFFF;
    var colorNotifications = 0xFFFFFF;

    var batteryThreshold = 30;

    var notificationSize = 100;

    var showBluetoothConnected = true;
    var showBluetoothNotConnected = true;
    var showNotifications = true;
    var mirrored = false;

    function initialize() {
        WatchFace.initialize();
        updateSettings();
    }

    // Load your resources here
    function onLayout(dc) {
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() {
    }

    // Update the view
    function onUpdate(dc) {
        var charging = false;
        var battery = 1.0;

        var displayHeight = System.getDeviceSettings().screenHeight;
        var displayWidth = System.getDeviceSettings().screenWidth;

        var fontTime = Graphics.FONT_SYSTEM_NUMBER_THAI_HOT;
        var fontDate = Graphics.FONT_MEDIUM;

		// Shifting screen a little to the right for devices like FR235 to
		// avoid numbers being cut off.
		var xOffset = (displayHeight == 180) ? 12 : 0;
		xOffset = mirrored ? -xOffset : xOffset;

		var xLine = displayWidth/2 + xOffset;
		var xTime = mirrored ?
				xLine + 10 :
				xLine - 10;
		var xDate = mirrored ?
				xLine - 10 :
				xLine + 10;
		var alignTime = mirrored ?
				Graphics.TEXT_JUSTIFY_LEFT :
				Graphics.TEXT_JUSTIFY_RIGHT;
		var alignDate = mirrored ?
				Graphics.TEXT_JUSTIFY_RIGHT :
				Graphics.TEXT_JUSTIFY_LEFT;

        var xIndicator = 0;
        var yIndicator = 0;

        // Get the current time and format it correctly
        var clockTime = System.getClockTime();
        var hours = (!System.getDeviceSettings().is24Hour && clockTime.hour>12) ?
                clockTime.hour-12 :
                clockTime.hour;
        var minutes = clockTime.min;

        var today = Time.Gregorian.info(Time.now(), Time.FORMAT_MEDIUM);

        // Set the background color then call to clear the screen
        // Workaround for unkown errors.
        try {
       		dc.setColor(
                	self.colorBackground,
                	self.colorBackground);
        }  catch (e) {
        	System.println("background");
        	System.println(e.getErrorMessage());
            e.printStackTrace();
        }
        dc.clear();

        /*
            Draw time.
        */
        try {
	        dc.setColor(
	                self.colorHour,
	                Graphics.COLOR_TRANSPARENT);
        }  catch (e) {
        	System.println("hour");
        	System.println(e.getErrorMessage());
            e.printStackTrace();
        }
        dc.drawText(
                xTime,
                displayHeight/2 - Graphics.getFontHeight(fontTime) + Graphics.getFontDescent(fontTime) - 3,
                fontTime,
                hours.format("%02d"),
                alignTime);
        try {
	        dc.setColor(
	                self.colorMinute,
	                Graphics.COLOR_TRANSPARENT);
        }  catch (e) {
        	System.println("minute");
        	System.println(e.getErrorMessage());
            e.printStackTrace();
        }
        dc.drawText(
                xTime,
                displayHeight/2 - Graphics.getFontDescent(fontTime) + 3,
                fontTime,
                minutes.format("%02d"),
                alignTime);

        /*
            Draw Date.
        */
        try {
	        dc.setColor(
	                self.colorDate,
	                Graphics.COLOR_TRANSPARENT);
        }  catch (e) {
        	System.println("date");
        	System.println(e.getErrorMessage());
            e.printStackTrace();
        }
        dc.drawText(
                xDate,
                displayHeight/2 - Graphics.getFontHeight(fontDate)/2,
                fontDate,
                today.day_of_week + " " + today.day,
                alignDate);

        /*
            Battery Indicator.
        */
        if (System.Stats has :charging) {
            charging = System.getSystemStats().charging;
        }
        if (System.Stats has :battery) {
            battery = System.getSystemStats().battery;
        }

        if (battery < batteryThreshold || charging) {
            drawBatterySymbol(
                    dc,
                    mirrored ?
                    	xDate-32 :
                    	xDate,
                    displayHeight/2 + 20,
                    battery);
        }

        /*
        	Notifications.
        */
        if (System.DeviceSettings has :phoneConnected) {
        	xIndicator = xDate;
        	yIndicator = 0.5 * (displayHeight - Graphics.getFontHeight(fontDate));
	        try {
		        dc.setColor(
		                self.colorNotifications,
		                Graphics.COLOR_TRANSPARENT);
	        }  catch (e) {
	        	System.println("notifications");
	        	System.println(e.getErrorMessage());
	            e.printStackTrace();
	        }
        	if (showNotifications &&
        			System.getDeviceSettings().notificationCount > 0) {
        		drawNotificationSymbol(
        				dc,
						xIndicator,
						yIndicator,
						notificationSize,
						mirrored);
			} else if (showBluetoothConnected &&
					System.getDeviceSettings().phoneConnected) {
				drawBluetoothSymbol(
						dc,
						xIndicator,
						yIndicator,
						notificationSize,
						mirrored,
						true);
			} else if (showBluetoothNotConnected &&
					!System.getDeviceSettings().phoneConnected) {
				drawBluetoothSymbol(
						dc,
						xIndicator,
						yIndicator,
						notificationSize,
						mirrored,
						false);
			}
        }

        /*
            Fancy line.
        */
        try {
	        dc.setColor(
	                self.colorLine,
	                self.colorBackground);
        }  catch (e) {
        	System.println("line");
        	System.println(e.getErrorMessage());
            e.printStackTrace();
        }
        dc.drawLine(
                xLine,
                displayHeight/2 - 52,
                xLine,
                displayHeight/2 + 61);
    }

    function updateSettings() {
        try {
            batteryThreshold = Application.getApp().getProperty("BatteryThreshold");
            showBluetoothConnected = Application.getApp().getProperty("ShowBluetoothConnected");
            showBluetoothNotConnected = Application.getApp().getProperty("ShowBluetoothNotConnected");
            showNotifications = Application.getApp().getProperty("ShowNotifications");
            notificationSize = Application.getApp().getProperty("NotificationSize");
            mirrored = Application.getApp().getProperty("Mirrored");
            colorBackground = Application.getApp().getProperty("ColorBackground");
            colorHour = Application.getApp().getProperty("ColorHour");
            colorMinute = Application.getApp().getProperty("ColorMinute");
            colorDate = Application.getApp().getProperty("ColorDate");
            colorLine = Application.getApp().getProperty("ColorLine");
            colorNotifications = Application.getApp().getProperty("ColorNotifications");
        } catch (e) {
            e.printStackTrace();
        }

        requestUpdate();
    }

    function drawBatterySymbol(dc, x, y, batteryLevel) {
        var width = 30;
        var height = 15;
        var penWidth = 1;

        dc.setPenWidth(penWidth);

        dc.setColor(
                Graphics.COLOR_DK_GRAY,
                Graphics.COLOR_DK_GRAY);
        dc.fillCircle(
                x+width,
                y+height/2-1,
                4);

        dc.setColor(
                Graphics.COLOR_BLACK,
                Graphics.COLOR_DK_GRAY);
        dc.fillRectangle(
                x,
                y,
                width,
                height);

        dc.setColor(
                Graphics.COLOR_GREEN,
                Graphics.COLOR_GREEN);
        dc.fillRectangle(
                x,
                y,
                width*batteryLevel/100.0,
                height);

        dc.setColor(
                Graphics.COLOR_DK_GRAY,
                Graphics.COLOR_DK_GRAY);
        dc.drawRectangle(
                x,
                y,
                width,
                height);
    }

    function drawNotificationSymbol(dc, x, y, size, mirrored) {
    	/*
    		Draw an envelope as symbol for phone notifications.
    	*/
    	// Workaround for unkown errors.
    	var scale = 100.0;
    	var height = 20.0;
    	var width = 30.0;
    	var middle = 15.0;
    	var middle_y = 6.0;

    	try {
    		scale = size / 100.0;
    		height = Math.floor(20.0 * scale);
    		width = Math.floor(30.0 * scale);
    		middle = Math.floor(30.0 / 2 * scale);
    		middle_y = Math.floor(height * 0.3);
    	} catch (e) {
    		System.println("drawnotification");
	        System.println(e.getErrorMessage());
            e.printStackTrace();
        }


    	if (mirrored) {
    		x = x - width;
    	}

    	dc.drawRectangle(
    		x,
    		y-height,
    		width,
    		height);
    	dc.drawLine(
    		x,
    		y-height,
    		x+middle,
    		y-middle_y);
    	dc.drawLine(
    		x+middle,
    		y-middle_y,
    		x+width-1,
    		y-height);
    }

    function drawBluetoothSymbol(dc, x, y, size, mirrored, active) {
    	// Workaround for unkown errors.
    	var scale = 100;
    	var height = 20.0;
    	var width = 10.0;
    	var x2 = 5.0;
    	var y14 = 5.0;
    	var y34 = 15.0;

    	try {
    		scale = size / 100.0;
    		height = Math.floor(20.0 * scale);
    		width = Math.floor(10.0 * scale);
    		x2 = Math.floor(width / 2.0);
    		y14 = y - Math.floor(height       / 4.0);
    		y34 = y - Math.floor(height * 3.0 / 4.0);
    	} catch (e) {
    		System.println("drawbt");
	        System.println(e.getErrorMessage());
            e.printStackTrace();
        }

		if (mirrored) {
			x = x - width;
		}

		dc.drawLine(
			x+x2,
			y,
			x+x2,
			y-height);

		dc.drawLine(
			x,
			y14,
			x+width,
			y34);

		dc.drawLine(
			x,
			y34,
			x+width,
			y14);

		dc.drawLine(
			x+x2,
			y-height+1,
			x+width,
			y34+1);

		dc.drawLine(
			x+x2,
			y-1,
			x+width,
			y14-1);

		if ( !active) {
			dc.setPenWidth(2);
			dc.drawLine(
				x,
				y,
				x + width,
				y - height);
			dc.setPenWidth(1);
		}
    }

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() {
    }

    // The user has just looked at their watch. Timers and animations may be started here.
    function onExitSleep() {
    }

    // Terminate any active timers and prepare for slow updates.
    function onEnterSleep() {
    }
}
